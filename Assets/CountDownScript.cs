﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownScript : MonoBehaviour
{
    public float currentTime = 0F;
    public float startingTime = 3F;
    public static bool timerIsRunning = false;
    public Text countDownText;
    private Action callbackWhenCountZero;


    public void Initialize()
    {
        countDownText.enabled = true;
        timerIsRunning = true;
    }

    public void Initialize(Action callback)
    {
        countDownText.enabled = true;
        timerIsRunning = true;
        callbackWhenCountZero = callback;
    }

    public void Reset()
    {
        countDownText.enabled = false;
        timerIsRunning = false;
        currentTime = startingTime;
    }

    private void Awake()
    {
        currentTime = startingTime;
        countDownText.color = Color.red;
    }

    void Update()
    {
        if (timerIsRunning && enabled)
        {
            if (currentTime > 0.0F)
            {
                currentTime -= Time.deltaTime;
                countDownText.text = Math.Ceiling(currentTime).ToString();
            }
            else
            {
                countDownText.text = "Start";
                timerIsRunning = false;
                StartCoroutine(ResetCourotine());
            }
        }
    }

    private IEnumerator ResetCourotine ()
    {
        yield return new WaitForSeconds(1f);
        countDownText.enabled = false;
        timerIsRunning = false;
        currentTime = startingTime;

        callbackWhenCountZero?.Invoke();
    }
}
