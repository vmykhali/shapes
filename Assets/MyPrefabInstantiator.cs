﻿using UnityEngine;
using Vuforia;
using System.Collections;
public class MyPrefabInstantiator : DefaultTrackableEventHandler
{
    //public Transform [] myModelPrefab;
    public Transform myModelPrefab;
    void Update()
    {
    }

    protected override void OnTrackingFound()
    {
        if (myModelPrefab != null)
        {
            //foreach (Transform prefab in myModelPrefab)
            //{
            //    ShapesGenerator(prefab);
            //}
            ShapesGenerator(myModelPrefab);
        }
    }

    protected void ShapesGenerator(Transform prefab)
    {
        Transform myModelTrf = GameObject.Instantiate(prefab) as Transform;
        myModelTrf.parent = mTrackableBehaviour.transform;
        myModelTrf.localPosition = new Vector3(Random.Range(-0.042f, 0.042f) , 0.035f, -0.045f);
        myModelTrf.localRotation = Quaternion.identity;
        //myModelTrf.localScale = new Vector3(0.0005f, 0.0005f, 0.0005f);
        myModelTrf.gameObject.SetActive(true);
    }
}