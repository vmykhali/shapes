﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShapeBehavior : MonoBehaviour
{
    public float MoveSpeed = 0.01f;

    private Vector3 mOrbitDirection;
    public bool isColorRandom = true;
    public Color shapeColor;
    public bool shapeIsAlive = true;

    private SpawnScript spawnObject;


    void Start()
    {
        spawnObject = GameObject.Find("Start").GetComponent<SpawnScript>();
        CubeSettings();
        shapeColor = new Color(Random.value, Random.value, Random.value, 1.0f);
        GetComponent<MeshRenderer>().material.color = shapeColor; // RANDOM COLOR WHEN PROJECT START
    }

    // Set initial cube settings
    private void CubeSettings()
    {
        // defining the orbit direction
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);
        float z = Random.Range(-1f, 1f);
        mOrbitDirection = new Vector3(x, y, z);
    }

    void Update()
    {
        MoveSpeed = spawnObject.moveSpeed;
        if (shapeIsAlive)
        {
            RotateAndMoveCube();
        }
    }
    private void RotateAndMoveCube()
    {
        // rotating around its axis
        transform.Rotate(mOrbitDirection * 30 * Time.deltaTime);

        float step = MoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z * 2f), step);
    }
}