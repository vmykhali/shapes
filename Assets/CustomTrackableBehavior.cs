﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CustomTrackableBehavior : DefaultTrackableEventHandler
{
    private bool startTheGame = true;
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        if (startTheGame)
        {
            CountDownScript count = mTrackableBehaviour.GetComponentInChildren<CountDownScript>();
            SpawnScript spawn = GetComponentInChildren<SpawnScript>();
            startTheGame = false;
            count.Initialize(spawn.Initialize);
        }
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
    }
}

