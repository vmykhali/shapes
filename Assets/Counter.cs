﻿using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    private ulong Iterator = 0;
    public Text OutputCount;

    private void Update()
    {
        OutputCount.text = Iterator.ToString();
    }

    public void Iterate ()
    {
        Iterator++;
    }
}
