﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickAndDestroy : MonoBehaviour
{
    public float cubeSize = 0.002f;
    public int cubesInRow = 5;

    float cubesPivotDistance;
    Vector3 cubesPivot;

    public float explosionForce = 0.02f;
    public float explosionRadius = 0.0001f;
    public float explosionUpward = 0.0001f;

    private Counter score;
    private Counter allShapes;

    void OnMouseDown()
    {
        explode();
    }

    // Use this for initialization
    void Start()
    {
        score = GameObject.Find("Score").GetComponent<Counter>();
        allShapes = GameObject.Find("Shapes").GetComponent<Counter>();
        cubesPivotDistance = cubeSize * cubesInRow / 2;
        cubesPivot = new Vector3(cubesPivotDistance, cubesPivotDistance, cubesPivotDistance);
    }

    public void explode()
    {
        //make object disappear
        gameObject.SetActive(false);

        //loop 3 times to create 5x5x5 pieces in x,y,z coordinates
        for (int x = 0; x < cubesInRow; x++)
        {
            for (int y = 0; y < cubesInRow; y++)
            {
                for (int z = 0; z < cubesInRow; z++)
                {
                    createPiece(x, y, z);
                }
            }
        }

        //get explosion position
        Vector3 explosionPos = transform.position;
        //get colliders in that position and radius
        Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
        //add explosion force to all colliders in that overlap sphere
        foreach (Collider hit in colliders)
        {
            //get rigidbody from collider object
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                //add explosion force to this body with given parameters
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius, explosionUpward);
            }
        }
        if (score && allShapes)
        {
            score.Iterate();
            allShapes.Iterate();
        }
        Destroy(gameObject);
    }

    void createPiece(int x, int y, int z)
    {

        //create piece
        GameObject piece;
        switch (gameObject.name.Replace("(Clone)", ""))
        {
            case "Cube":
                piece = GameObject.CreatePrimitive(PrimitiveType.Cube);
                break;
            case "Cylinder":
                piece = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                break;
            case "Sphere":
                piece = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                break;
            default:
                piece = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                break;
        }
        //set piece position and scale
        piece.transform.position = transform.position + new Vector3(cubeSize * x, cubeSize * y, cubeSize * z) - cubesPivot;
        piece.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        //add rigidbody and set mass
        piece.AddComponent<Rigidbody>();
        piece.GetComponent<Rigidbody>().mass = cubeSize;
        piece.GetComponent<Rigidbody>().useGravity = false;
        piece.GetComponent<MeshRenderer>().material.color = gameObject.GetComponent<ShapeBehavior>().shapeColor;
        Destroy(piece, 2f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Finish")
        {
            if (score && allShapes)
            {
                allShapes.Iterate();
            }
            Destroy(gameObject);
        }
    }
}
