﻿using System.Collections;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    public Transform [] mCubeObj;
    internal bool spawnStart = true;
    
    public float moveSpeed = 0.01f;
    float maxSpeed = 10f;
    public float encreaseSpeed = 20;
    private float accelerationTime = 0;
    private float time;
    public float defaultTime = 10f;

    public float mTimeToSpawn = 1f;
    private float minTimeToSpawn = 0.3f;
    private void Start()
    {
        time = defaultTime;
    }
    void EncreaseSpeed()
    {
        moveSpeed = moveSpeed + moveSpeed * 20 / 100;
        mTimeToSpawn = mTimeToSpawn - mTimeToSpawn * 20 / 100;
        if (moveSpeed > maxSpeed)
        {
            moveSpeed = maxSpeed;
        }
        if (mTimeToSpawn < minTimeToSpawn)
        {
            mTimeToSpawn = minTimeToSpawn;
        }
        time = defaultTime;
    }

    private void Update()
    {
        time -= Time.deltaTime;

        if (time < accelerationTime)
        {
            EncreaseSpeed();
        }
    }

    public void Initialize()
    {
        StartCoroutine(SpawnLoop());
    }

    // Loop Spawning cube elements
    private IEnumerator SpawnLoop()
    {
        yield return new WaitForSeconds(0.2f);

        int shapesIndex = 0;
        while (spawnStart)
        {
            SpawnElement(mCubeObj[shapesIndex]); // Spawning the 
            shapesIndex++;
            if (shapesIndex == mCubeObj.Length)
            {
                shapesIndex = 0;//if end of shapes Array make shapes index 0 for queue output of shapes
            }
            yield return new WaitForSeconds(Random.Range(mTimeToSpawn, mTimeToSpawn * 2));
        }
    }
    // Spawn a cube
    private Transform SpawnElement(Transform shapeObject)
    {
        Vector3 coordinatesOfCube = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Transform cube = Instantiate(shapeObject, coordinatesOfCube, Quaternion.identity);
        cube.GetComponent<ShapeBehavior>().MoveSpeed = moveSpeed;
        return cube;
    }
}